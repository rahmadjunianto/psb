<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pendaftaran extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pendaftaran_model');
		$this->load->model('Ibu_model');
		$this->load->model('Ayah_model');
	}
	public function index()
	{
		// 	$this->load->model('Pengaturan_model');
		// 	$data['whatsapp'] = $this->Pengaturan_model->get_whatsapp();

		$this->load->view('header');

		$this->load->view('pendaftaran');

		$this->load->view('footer');
	}

	public function kirim()
	{
		$this->db->trans_start();
		$data = [];
		$data['Nama_Admin'] = $this->input->post('nama');
		$data['Username'] = $this->input->post('username');
		$data['Password'] = $this->input->post('password');
		$this->db->insert('tb_admin', $data);
		$insert_id = $this->db->insert_id();
		$data = [];
		$no_pendaftaran = $this->Pendaftaran_model->get_no_pendaftaran();
		$data['NIK'] = $this->input->post('nik');
		$data['Nama'] = $this->input->post('nama');
		$data['Tempat_Lahir'] = $this->input->post('tempat_lahir');
		$data['Tanggal_Lahir'] = $this->input->post('tanggal_lahir');
		$data['NISN'] = $this->input->post('nisn');
		$data['Agama'] = $this->input->post('agama');
		$data['Jenis_Kelamin'] = $this->input->post('jenis_kelamin');
		$data['Sekolah_Asal'] = $this->input->post('sekolah_asal');
		$data['Alamat'] = $this->input->post('alamat_asal');
		$data['Kecamatan'] = $this->input->post('kecamatan');
		$data['Kabupaten_Kota'] = $this->input->post('kabupaten_kota');
		$data['Tahun_Lulus'] = $this->input->post('tahun_lulus');
		$data['Nomor_Pendaftaran'] = $no_pendaftaran;
		$data['Id_Admin'] = $insert_id;
		$this->Pendaftaran_model->insert($data);
		$data = [];
		$data['Nama_Ibu'] = $this->input->post('i_nama');
		$data['Tempat_Lahir_Ibu'] = $this->input->post('i_tempat_lahir');
		$data['Tanggal_Lahir_Ibu'] = $this->input->post('i_tanggal_lahir');
		$data['RT'] = $this->input->post('i_rt');
		$data['RW'] = $this->input->post('i_rw');
		$data['Alamat'] = $this->input->post('i_alamat_asal');
		$data['Kecamatan'] = $this->input->post('i_kecamatan');
		$data['Kabupaten_Kota'] = $this->input->post('i_kabupaten_kota');
		$data['Nomor_Telepon'] = $this->input->post('i_no_telepon');
		$data['Nomor_Pendaftaran'] = $no_pendaftaran;
		$this->Ibu_model->insert($data);
		$data = [];
		$data['Nama_Ayah'] = $this->input->post('a_nama');
		$data['Tempat_Lahir_Ayah'] = $this->input->post('a_tempat_lahir');
		$data['Tanggal_Lahir_Ayah'] = $this->input->post('a_tanggal_lahir');
		$data['RT'] = $this->input->post('a_rt');
		$data['RW'] = $this->input->post('a_rw');
		$data['Alamat_Ayah'] = $this->input->post('a_alamat_asal');
		$data['Kecamatan'] = $this->input->post('a_kecamatan');
		$data['Kabupaten_Kota'] = $this->input->post('a_kabupaten_kota');
		$data['Nomor_Telepon'] = $this->input->post('a_no_telepon');
		$data['Nomor_Pendaftaran'] = $no_pendaftaran;
		$this->Ayah_model->insert($data);
		$data['Nomor_Pendaftaran'] = $no_pendaftaran;
		$this->db->trans_complete();
		$this->load->view('header');

		$this->load->view('thank_you', $data);

		$this->load->view('footer', $data);
	}
	function print($nisn)
	{
		$data['siswa'] = $this->db->query("SELECT * FROM tb_pendaftaran where Nomor_Pendaftaran='$nisn'")->row();
		$data['ayah'] = $this->db->query("SELECT * FROM tb_orang_tua_ayah where Nomor_Pendaftaran='$nisn'")->row();
		$data['ibu'] = $this->db->query("SELECT * FROM tb_orang_tua_ibu where Nomor_Pendaftaran='$nisn'")->row();
		$this->load->view('bukti', $data);
	}

	function edit($id)
	{
		$data['siswa'] = $this->db->query("SELECT * FROM tb_pendaftaran where Id_Admin='$id'")->row();
		$nisn = $data['siswa']->Nomor_Pendaftaran ?? null;
		$data['ayah'] = $this->db->query("SELECT * FROM tb_orang_tua_ayah where Nomor_Pendaftaran='$nisn'")->row();
		$data['ibu'] = $this->db->query("SELECT * FROM tb_orang_tua_ibu where Nomor_Pendaftaran='$nisn'")->row();

		$this->load->view('header');

		$this->load->view('form_pendaftaran', $data);

		$this->load->view('footer');
	}
	function update()
	{
		$this->db->trans_start();
		$no_pendaftaran = $this->input->post('Nomor_Pendaftaran')!=null? $this->input->post('Nomor_Pendaftaran'): $this->Pendaftaran_model->get_no_pendaftaran();
		// echo $no_pendaftaran ;
		// die();
		$data['NIK'] = $this->input->post('nik');
		$data['Nama'] = $this->input->post('nama');
		$data['Tempat_Lahir'] = $this->input->post('tempat_lahir');
		$data['Tanggal_Lahir'] = $this->input->post('tanggal_lahir');
		$data['NISN'] = $this->input->post('nisn');
		$data['Agama'] = $this->input->post('agama');
		$data['Jenis_Kelamin'] = $this->input->post('jenis_kelamin');
		$data['Sekolah_Asal'] = $this->input->post('sekolah_asal');
		$data['Alamat'] = $this->input->post('alamat_asal');
		$data['Kecamatan'] = $this->input->post('kecamatan');
		$data['Kabupaten_Kota'] = $this->input->post('kabupaten_kota');
		$data['Tahun_Lulus'] = $this->input->post('tahun_lulus');
		$data['Nomor_Telepon'] = $this->input->post('Nomor_Telepon');

		$query = $this->db->query("SELECT * FROM tb_pendaftaran WHERE Id_Admin=?", array($this->input->post('Id_Admin')))->row();
		if ($query) {
			$data['Nomor_Pendaftaran'] = $this->input->post('Nomor_Pendaftaran');
			$this->db->where('Id_Admin', $this->input->post('Id_Admin'));
			$this->db->update('tb_pendaftaran', $data);
		} else {
			$data['Id_Admin'] = $this->input->post('Id_Admin');
			$data['Nomor_Pendaftaran'] = $this->Pendaftaran_model->get_no_pendaftaran();
			$this->Pendaftaran_model->insert($data);
		}

		$data = [];
		$data['Nama_Ibu'] = $this->input->post('i_nama');
		$data['Tempat_Lahir_Ibu'] = $this->input->post('i_tempat_lahir');
		$data['Tanggal_Lahir_Ibu'] = $this->input->post('i_tanggal_lahir');
		$data['RT'] = $this->input->post('i_rt');
		$data['RW'] = $this->input->post('i_rw');
		$data['Alamat'] = $this->input->post('i_alamat_asal');
		$data['Kecamatan'] = $this->input->post('i_kecamatan');
		$data['Kabupaten_Kota'] = $this->input->post('i_kabupaten_kota');
		$data['Nomor_Telepon'] = $this->input->post('i_no_telepon');

		$query = $this->db->query("SELECT * FROM tb_orang_tua_ibu WHERE Nomor_Pendaftaran=?", array($no_pendaftaran))->row();
		if ($query) {
			$this->db->where('Nomor_Pendaftaran', $this->input->post('Nomor_Pendaftaran'));
			$this->db->update('tb_orang_tua_ibu', $data);
		} else {
			$data['Nomor_Pendaftaran'] = $no_pendaftaran;
			$this->Ibu_model->insert($data);
		}

		$data = [];
		$data['Nama_Ayah'] = $this->input->post('a_nama');
		$data['Tempat_Lahir_Ayah'] = $this->input->post('a_tempat_lahir');
		$data['Tanggal_Lahir_Ayah'] = $this->input->post('a_tanggal_lahir');
		$data['RT'] = $this->input->post('a_rt');
		$data['RW'] = $this->input->post('a_rw');
		$data['Alamat_Ayah'] = $this->input->post('a_alamat_asal');
		$data['Kecamatan'] = $this->input->post('a_kecamatan');
		$data['Kabupaten_Kota'] = $this->input->post('a_kabupaten_kota');
		$data['Nomor_Telepon'] = $this->input->post('a_no_telepon');

		$query = $this->db->query("SELECT * FROM tb_orang_tua_ayah WHERE Nomor_Pendaftaran=?", array($no_pendaftaran))->row();
		if ($query) {
			$this->db->where('Nomor_Pendaftaran', $this->input->post('Nomor_Pendaftaran'));
			$this->db->update('tb_orang_tua_ayah', $data);
		} else {
			$data['Nomor_Pendaftaran'] = $no_pendaftaran;
			$this->Ayah_model->insert($data);
		}
		$this->db->trans_complete();

		$this->session->set_flashdata('sukses', 'Berhasil Update');
		if ($this->session->userdata('id') != 123456789) {
			redirect($_SERVER['HTTP_REFERER']);
		} else {
			redirect('peserta');
		}
	}
	function hapus($id)
	{
		$this->db->where('Nomor_Pendaftaran', $id);
		$this->db->delete('tb_pendaftaran');
		redirect($_SERVER['HTTP_REFERER']);
	}
}
