<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengumuman extends CI_Controller {

	public function __construct() {
        parent::__construct();
		$this->load->model('Pendaftaran_model');
	}
	public function index()
	{
		// echo $this->input->get('NISN');die();
        $this->db->where('NISN', $this->input->get('NISN'));
		$data['siswa'] = $this->db->get('tb_pendaftaran')->row();
		$data['nisn'] = $this->input->get('NISN');

		$this->load->view('header');

		$this->load->view('pengumuman', $data);

		$this->load->view('footer', $data);
	}
}
