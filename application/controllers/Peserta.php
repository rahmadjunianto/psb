<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta extends CI_Controller {

	public function __construct() {
        parent::__construct();
		$this->load->model('Pendaftaran_model');
	}
	public function index()
	{
		// echo $this->input->get('NISN');die();
		$data['siswa'] = $this->db->get('tb_pendaftaran')->result();

		$this->load->view('header');

		$this->load->view('peserta', $data);

		$this->load->view('footer', $data);
	}
}
