<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
        parent::__construct();
		$this->load->model('Pendaftaran_model');
	}
	public function index()
	{

		$this->load->view('header');

		$this->load->view('login');

		$this->load->view('footer');
	}
	function prosesLogin() {

		if (isset($_POST['Username']) && isset($_POST['Password'])) {
			$username = $this->input->post('Username');
			$password = $this->input->post('Password');
			$query=$this->db->query("SELECT *
										FROM tb_admin
										WHERE Username=? and Password=?",array($username,$password))->row();
			if(!empty($query) ){
				$this->session->set_userdata('id',$query->Id_Admin);
				$this->session->set_userdata('nama',$query->Nama_Admin);
				// echo $this->session->userdata('id');
				redirect('/');
			}else{
				$this->session->set_flashdata('warning', 'Username atau Password tidak sesuai');
				redirect('login');
			}
		} else {
			// echo "asd";
			redirect('login');
		}
	}
	function logout()
	{
		// $this->session->sess_destroy();
		session_destroy();
		redirect('/');
	}

	public function registrasi()
	{

		$this->load->view('header');

		$this->load->view('registrasi');

		$this->load->view('footer');
	}
	public function lupa_password()
	{

		$this->load->view('header');

		$this->load->view('lupa_password');

		$this->load->view('footer');
	}
	function prosesRegistrasi() {
		if (isset($_POST['Username']) && isset($_POST['Password'])) {
			$username = $this->input->post('Username');
			$password = $this->input->post('Password');
			$query=$this->db->query("SELECT * FROM tb_admin WHERE Username=?",array($username))->row();
			if(!empty($query) ){
				$this->session->set_flashdata('warning', 'Username sudah Ada');
				redirect('login/registrasi');
			}else{
				$data = [];
				$data['Nama_Admin'] = $this->input->post('Username');
				$data['Password'] = $this->input->post('Password');
				$this->db->insert('tb_admin', $data);
				$insert_id = $this->db->insert_id();
				$this->session->set_userdata('id',$insert_id);
				$this->session->set_userdata('nama',$query->Nama_Admin);
				redirect('pendaftaran/edit/'.$this->session->userdata('id'));
			}
		}

	}
	function prosesReset() {
		$username = $this->input->post('Username');
		$password = $this->input->post('Password');
		$query=$this->db->query("SELECT * FROM tb_admin WHERE Username=?",array($username))->row();
		if(!empty($query) ){
			$data['Password'] = $password ;
			$this->db->where('Username', $this->input->post('Username'));
			$this->db->update('tb_admin', $data);
			$this->session->set_flashdata('warning', 'Berhasil Reset Password');
			redirect('login/registrasi');
		}else{
			$this->session->set_flashdata('warning', 'Username Tidak Ada');
			redirect('login/lupa_password');
		}

	}
}
