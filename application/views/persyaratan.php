<div id="persyaratan" class="grid-container persyaratan">
  <div class="grid-x align-center">
    <div class="cell large-8 rounded shadow grey-border" style="margin-top:10px">
      <br><br>
      <br><br>

				<?php if (!$this->session->userdata('id')) { ?>
        <h4 class="text-center">
          Selamat Datang di Form Pendaftaran Siswa Baru <br>
          SMP Unggulan Al-Falah
        </h4>

        <h4 class="text-center">
					<a   href="<?php echo base_url(); ?>login/registrasi" class="submit button primary shadow rounded whatsapp teal text-center">Daftar Sekarang</a>
</h4>
				<?php } else { ?>
					<?php if ($this->session->userdata('id')!=123456789) { ?>
              <h4 class="text-center">
              Selamat Datang <?=$this->session->userdata('nama')?> PSB Online <br>
              SMP Unggulan Al-Falah

              </h4>
					<?php } else { ?>
              <h4 class="text-center">
              Selamat Datang <?=$this->session->userdata('nama')?> Admin PSB Online<br>
              SMP Unggulan Al-Falah

              </h4>

          <?php } ?>

				<?php } ?>
      <ul>
      </ul>
    </div>
  </div>
</div><!-- .persyaratan -->