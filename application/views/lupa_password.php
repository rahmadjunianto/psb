<div class="grid-container pengumuman section">
  <div class="grid-x grid-margin-x grid-padding-x">
    <div class="cell large-12">
        <!-- Judul -->
        <h1 class="text-center section-title">Reset Password</h1>

		<form action="<?=base_url()?>login/prosesReset" method="post" accept-charset="utf-8">
            <?=$this->session->userdata('warning');?>

            <label for="nisn" class="">Username </label>
            <input required type="text" name="Username" maxlength="100" size="50" placeholder="Username">

            <label for="nisn" class="">Password Baru</label>
            <input required type="password" name="Password"  maxlength="100" size="50" placeholder="Password">

            <div class="grid-x">
    			<div class="cell larga-12 text-center">
    			</div>
    		</div>

    		<div class="grid-x">
    			<div class="cell larga-12 text-center">
    				<input required type="submit" value="Reset" class="submit button primary shadow rounded whatsapp teal" id="submit">
    			</div>
    		</div>
        </form>

    </div>
  </div>
</div><!-- .pengumuman -->