<div class="grid-container pengumuman section">
  <div class="grid-x grid-margin-x grid-padding-x">
    <div class="cell large-12">
      <h1 class="text-center section-title">Data Peserta</h1>
      <table>
        <thead>
          <tr>
            <th align="center" width="5%">No</th>
            <th align="center" width="15%">No Pendaftaran</th>
            <th align="center" width="">Nama</th>
            <th align="center" width="15%">Jenis Kelamin</th>
            <th align="center" style="align-items: center;" width="25%">Aksi</th>
          </tr>
        </thead>
        <tbody>
            <?php $no=1; foreach ($siswa as $s) { ?>
            <tr>
              <td><?=$no++?></td>
              <td><?php echo $s->Nomor_Pendaftaran; ?></td>
              <td><?php echo $s->Nama; ?></td>
              <td><?php echo $s->Jenis_Kelamin; ?></td>
              <td align="center">

              <a style="margin: 0px;" class="submit button primary shadow rounded teal" href="<?=base_url()?>pendaftaran/edit/<?=$s->Id_Admin?>" class="noPrint">
              Detail
              </a>
              <a onclick="return confirm('Are you sure you want to delete this item?');" style="margin: 0px;" class="submit button warning shadow rounded teal" href="<?=base_url()?>pendaftaran/hapus/<?=$s->Nomor_Pendaftaran?>" class="noPrint">
              Hapus
              </a>

              </td>
            </tr>
            <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div><!-- .pengumuman -->


<script>
  printDiv("myDiv");

function printDiv(id){
        var printContents = document.getElementById(id).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
}
</script>