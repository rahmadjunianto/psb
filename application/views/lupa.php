<div class="grid-container pendaftaran section">
	<div class="grid-x grid-margin-x grid-padding-x">
		<div class="cell large-12">
			<h1 class="text-center section-title">PENDAFTARAN</h1>
			<form action="<?=base_url()?>/pendaftaran/kirim" method="post" accept-charset="utf-8">

				<div class="grid-x grid-margin-x">
					<div class="cell large-4">
						<h5>A. BIODATA SISWA</h5>
						<label for="nik" class="">NIK</label><input required type="number" name="nik" value="" id="nik" maxlength="100" size="50">
						<label for="nama" class="">Nama</label><input required type="text" name="nama" value="" id="nama" maxlength="100" size="50">
						<label for="tempat_lahir" class="">Tempat Lahir</label><input required type="text" name="tempat_lahir" value="" id="tempat_lahir" maxlength="100" size="50">
						<label for="tanggal_lahir" class="">Tanggal Lahir</label><input required type="date" name="tanggal_lahir" value="" id="tanggal_lahir" maxlength="100" size="50">
						<label for="nisn" class="">NISN</label><input required type="number" name="nisn" value="" id="nisn" maxlength="100" size="50">
						<label for="agama" class="">Agama</label><select required name="agama">
							<option value="Islam" selected="selected">Islam</option>
							<option value="Kristen">Kristen</option>
							<option value="Hindu">Hindu</option>
							<option value="Buddha">Buddha</option>
							<option value="Konghuchu">Konghuchu</option>
						</select>
						<label for="jenis_kelamin" class="">Jenis Kelamin</label><select required name="jenis_kelamin">
							<option value="Laki-Laki">Laki Laki</option>
							<option value="Perempuan">Perempuan</option>
						</select>
						<label for="sekolah_asal" class="">Sekolah Asal</label><input required type="text" name="sekolah_asal" value="" id="sekolah_asal" maxlength="100" size="50">
						<label for="alamat_asal" class="">Alamat</label><textarea required name="alamat_asal" cols="20" rows="2" id="alamat_asal" maxlength="100" size="50"></textarea>

						<label for="kecamatan" class="">Kecamatan</label><input required type="text" name="kecamatan" value="" id="kecamatan" maxlength="100" size="50">
						<label for="kabupaten_kota" class="">Kabupaten /Kota</label><input required type="text" name="kabupaten_kota" value="" id="kabupaten_kota" maxlength="100" size="50">
						<label for="nama" class="">Tahun Lulus</label><input required type="number" name="tahun_lulus" value="" id="tahun_lulus" maxlength="100" size="50">
					</div>
					<div class="cell large-4">
						<h5>B. BIODATA AYAH</h5>
						<label for="nama" class="">Nama</label><input required type="text" name="a_nama" value="" id="nama" maxlength="100" size="50">
						<label for="tempat_lahir" class="">Tempat Lahir</label><input required type="text" name="a_tempat_lahir" value="" id="tempat_lahir" maxlength="100" size="50">
						<label for="tanggal_lahir" class="">Tanggal Lahir</label><input required type="date" name="a_tanggal_lahir" value="" id="tanggal_lahir" maxlength="100" size="50">
						<label for="rt" class="">RT</label><input required type="text" name="a_rt" value="" id="rt" maxlength="100" size="50">
						<label for="rw" class="">RW</label><input required type="text" name="a_rw" value="" id="rw" maxlength="100" size="50">
						<label for="alamat_asal" class="">Alamat</label><textarea required name="a_alamat_asal" cols="20" rows="2" id="alamat_asal" maxlength="100" size="50"></textarea>
						<label for="kecamatan" class="">Kecamatan</label><input required type="text" name="a_kecamatan" value="" id="kecamatan" maxlength="100" size="50">
						<label for="kabupaten_kota" class="">Kabupaten /Kota</label><input required type="text" name="a_kabupaten_kota" value="" id="kabupaten_kota" maxlength="100" size="50">
						<label for="nama" class="">Nomor Telepon</label><input required type="number" name="a_no_telepon" value="" id="no_telepon" maxlength="100" size="50">
					</div>
					<div class="cell large-4">
						<h5>C. BIODATA IBU</h5>
						<label for="nama" class="">Nama</label><input required type="text" name="i_nama" value="" id="nama" maxlength="100" size="50">
						<label for="tempat_lahir" class="">Tempat Lahir</label><input required type="text" name="i_tempat_lahir" value="" id="tempat_lahir" maxlength="100" size="50">
						<label for="tanggal_lahir" class="">Tanggal Lahir</label><input required type="date" name="i_tanggal_lahir" value="" id="tanggal_lahir" maxlength="100" size="50">
						<label for="rt" class="">RT</label><input required type="text" name="i_rt" value="" id="rt" maxlength="100" size="50">
						<label for="rw" class="">RW</label><input required type="text" name="i_rw" value="" id="rw" maxlength="100" size="50">
						<label for="alamat_asal" class="">Alamat</label><textarea required name="i_alamat_asal" cols="20" rows="2" id="alamat_asal" maxlength="100" size="50"></textarea>
						<label for="kecamatan" class="">Kecamatan</label><input required type="text" name="i_kecamatan" value="" id="kecamatan" maxlength="100" size="50">
						<label for="kabupaten_kota" class="">Kabupaten /Kota</label><input required type="text" name="i_kabupaten_kota" value="" id="kabupaten_kota" maxlength="100" size="50">
						<label for="nama" class="">Nomor Telepon</label><input required type="number" name="i_no_telepon" value="" id="no_telepon" maxlength="100" size="50">
					</div>
				</div>

				<div class="grid-x">
					<div class="cell larga-12 text-center">
						<input required type="submit" name="siswa" value="KIRIM" class="submit button primary shadow rounded whatsapp teal" id="submit">

					</div>
				</div>
			</form>

		</div>
	</div>
</div>