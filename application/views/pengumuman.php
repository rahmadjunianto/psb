<div class="grid-container pengumuman section">
  <div class="grid-x grid-margin-x grid-padding-x">
    <div class="cell large-12">
      <h1 class="text-center section-title">PENGUMUMAN</h1>
			<form action="<?=base_url()?>pengumuman" method="get" accept-charset="utf-8">
      <label for="nisn" class="">NISN</label>
      <input required type="number" name="NISN" value="<?=$nisn?>" id="nisn" maxlength="100" size="50" placeholder="Masukkan nomor NISN">
				<div class="grid-x">
					<div class="cell larga-12 text-center">
						<input required type="submit" value="Cari" class="submit button primary shadow rounded whatsapp teal" id="submit">
					</div>
				</div>
      </form>
      <?php if ($siswa) { ?>
      <table>
        <thead>
          <tr>
            <th width="5%">No</th>
            <th width="15%">No Pendaftaran</th>
            <th width="">Nama</th>
            <th width="15%">Jenis Kelamin</th>
          </tr>
        </thead>
        <tbody>
            <tr>
              <td>1</td>
              <td><?php echo $siswa->Nomor_Pendaftaran; ?></td>
              <td><?php echo $siswa->Nama; ?></td>
              <td><?php echo $siswa->Jenis_Kelamin; ?></td>
            </tr>
        </tbody>
      </table>
      <?php } ?>
    </div>
  </div>
</div><!-- .pengumuman -->


<script>
  printDiv("myDiv");

function printDiv(id){
        var printContents = document.getElementById(id).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
}
</script>