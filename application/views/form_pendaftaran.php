<div class="grid-container pendaftaran section">
	<div class="grid-x grid-margin-x grid-padding-x">
		<div class="cell large-12">
			<h1 class="text-center section-title">PENDAFTARAN</h1>
			<form action="<?= base_url() ?>/pendaftaran/update" method="post" accept-charset="utf-8">
				<h3 style="color: green;"><?= $this->session->userdata('sukses'); ?></h3>
				<br>
				<div class="grid-x grid-margin-x">
					<div class="cell large-4">
						<h5>A. BIODATA SISWA</h5>
						<input type="hidden" name="Id_Admin" value="<?= $siswa->Id_Admin ?? $this->session->userdata('id') ?>">
						<input type="hidden" name="Nomor_Pendaftaran" value="<?= $siswa->Nomor_Pendaftaran ?? '' ?>">
						<label for="nik" class="">NIK</label><input required type="number" name="nik" value="<?= $siswa->NIK ?? '' ?>" id="nik" maxlength="100" size="50">
						<label for="nama" class="">Nama</label><input required type="text" name="nama" value="<?= $siswa->Nama ?? '' ?>" id="nama" maxlength="100" size="50">
						<label for="tempat_lahir" class="">Tempat Lahir</label><input required type="text" name="tempat_lahir" value="<?= $siswa->Tempat_Lahir ?? '' ?>" id="tempat_lahir" maxlength="100" size="50">
						<label for="tanggal_lahir" class="">Tanggal Lahir</label><input required type="date" name="tanggal_lahir" value="<?= $siswa->Tanggal_Lahir ?? '' ?>" id="tanggal_lahir" maxlength="100" size="50">
						<label for="nisn" class="">NISN</label><input required type="number" name="nisn" value="<?= $siswa->NISN ?? '' ?>" id="nisn" maxlength="100" size="50">
						<label for="agama" class="">Agama</label><select required name="agama">
							<option <?= $siswa && $siswa->Agama == 'Islam' ? 'selected' : '' ?> value="Islam">Islam</option>
							<option <?= $siswa && $siswa->Agama == 'Kristen' ? 'selected' : '' ?> value="Kristen">Kristen</option>
							<option <?= $siswa && $siswa->Agama == 'Hindu' ? 'selected' : '' ?> value="Hindu">Hindu</option>
							<option <?= $siswa && $siswa->Agama == 'Buddha' ? 'selected' : '' ?> value="Buddha">Buddha</option>
							<option <?= $siswa && $siswa->Agama == 'Konghuchu' ? 'selected' : '' ?> value="Konghuchu">Konghuchu</option>
						</select>
						<label for="jenis_kelamin" class="">Jenis Kelamin</label><select required name="jenis_kelamin">
							<option <?= $siswa && $siswa->Jenis_Kelamin == 'Laki-Laki' ? 'selected' : '' ?> value="Laki-Laki">Laki Laki</option>
							<option <?= $siswa && $siswa->Jenis_Kelamin == 'Perempuan' ? 'selected' : '' ?> value="Perempuan">Perempuan</option>
						</select>
						<label for="sekolah_asal" class="">Sekolah Asal</label><input required type="text" name="sekolah_asal" value="<?= $siswa->Sekolah_Asal ?? '' ?>" id="sekolah_asal" maxlength="100" size="50">
						<label for="alamat_asal" class="">Alamat</label><textarea required name="alamat_asal" cols="20" rows="2" id="alamat_asal" maxlength="100" size="50"><?= $siswa->Alamat ?? '' ?></textarea>

						<label for="kecamatan" class="">Kecamatan</label><input required type="text" name="kecamatan" value="<?= $siswa->Kecamatan ?? '' ?>" id="kecamatan" maxlength="100" size="50">
						<label for="kabupaten_kota" class="">Kabupaten /Kota</label><input required type="text" name="kabupaten_kota" value="<?= $siswa->Kabupaten_Kota ?? '' ?>" id="kabupaten_kota" maxlength="100" size="50">
						<label for="nama" class="">Tahun Lulus</label><input required type="number" name="tahun_lulus" value="<?= $siswa->Tahun_Lulus ?? '' ?>" id="tahun_lulus" maxlength="100" size="50">
						<label for="nama" class="">Nomor Telepon</label><input required type="number" name="Nomor_Telepon" value="<?= $siswa->Nomor_Telepon ?? '' ?>" id="no_telepon" maxlength="100" size="50">
					</div>
					<div class="cell large-4">
						<h5>B. BIODATA AYAH</h5>
						<label for="nama" class="">Nama</label><input required type="text" name="a_nama" value="<?= $ayah->Nama_Ayah ?? '' ?>" id="nama" maxlength="100" size="50">
						<label for="tempat_lahir" class="">Tempat Lahir</label><input required type="text" name="a_tempat_lahir" value="<?= $ayah->Tempat_Lahir_Ayah ?? '' ?>" id="tempat_lahir" maxlength="100" size="50">
						<label for="tanggal_lahir" class="">Tanggal Lahir</label><input required type="date" name="a_tanggal_lahir" value="<?= $ayah->Tanggal_Lahir_Ayah ?? '' ?>" id="tanggal_lahir" maxlength="100" size="50">
						<label for="rt" class="">RT</label><input required type="text" name="a_rt" value="<?= $ayah->RT ?? '' ?>" id="rt" maxlength="100" size="50">
						<label for="rw" class="">RW</label><input required type="text" name="a_rw" value="<?= $ayah->RW ?? '' ?>" id="rw" maxlength="100" size="50">
						<label for="alamat_asal" class="">Alamat</label><textarea required name="a_alamat_asal" cols="20" rows="2" id="alamat_asal" maxlength="100" size="50"><?= $ayah->Alamat_Ayah ?? '' ?></textarea>
						<label for="kecamatan" class="">Kecamatan</label><input required type="text" name="a_kecamatan" value="<?= $ayah->Kecamatan ?? '' ?>" id="kecamatan" maxlength="100" size="50">
						<label for="kabupaten_kota" class="">Kabupaten /Kota</label><input required type="text" name="a_kabupaten_kota" value="<?= $ayah->Kabupaten_Kota ?? '' ?>" id="kabupaten_kota" maxlength="100" size="50">
						<label for="nama" class="">Nomor Telepon</label><input required type="number" name="a_no_telepon" value="<?= $ayah->Nomor_Telepon ?? '' ?>" id="no_telepon" maxlength="100" size="50">
					</div>
					<div class="cell large-4">
						<h5>C. BIODATA IBU</h5>
						<label for="nama" class="">Nama</label><input required type="text" name="i_nama" value="<?= $ibu->Nama_Ibu ?? '' ?>" id="nama" maxlength="100" size="50">
						<label for="tempat_lahir" class="">Tempat Lahir</label><input required type="text" name="i_tempat_lahir" value="<?= $ibu->Tempat_Lahir_Ibu ?? '' ?>" id="tempat_lahir" maxlength="100" size="50">
						<label for="tanggal_lahir" class="">Tanggal Lahir</label><input required type="date" name="i_tanggal_lahir" value="<?= $ibu->Tanggal_Lahir_Ibu ?? '' ?>" id="tanggal_lahir" maxlength="100" size="50">
						<label for="rt" class="">RT</label><input required type="text" name="i_rt" value="<?= $ibu->RT ?? '' ?>" id="rt" maxlength="100" size="50">
						<label for="rw" class="">RW</label><input required type="text" name="i_rw" value="<?= $ibu->RW ?? '' ?>" id="rw" maxlength="100" size="50">
						<label for="alamat_asal" class="">Alamat</label><textarea required name="i_alamat_asal" cols="20" rows="2" id="alamat_asal" maxlength="100" size="50"><?= $ibu->Alamat ?? '' ?></textarea>
						<label for="kecamatan" class="">Kecamatan</label><input required type="text" name="i_kecamatan" value="<?= $ibu->Kecamatan ?? '' ?>" id="kecamatan" maxlength="100" size="50">
						<label for="kabupaten_kota" class="">Kabupaten /Kota</label><input required type="text" name="i_kabupaten_kota" value="<?= $ibu->Kabupaten_Kota ?? '' ?>" id="kabupaten_kota" maxlength="100" size="50">
						<label for="nama" class="">Nomor Telepon</label><input required type="number" name="i_no_telepon" value="<?= $ibu->Nomor_Telepon ?? '' ?>" id="no_telepon" maxlength="100" size="50">
					</div>
				</div>

				<div class="grid-x">
					<div class="cell larga-12 text-center">
						<input required type="submit" name="siswa" value="Simpan" class="submit button primary shadow rounded whatsapp teal" id="submit">
						<?php if ($siswa) { ?>
							<a target="_blank" class="submit button primary shadow rounded teal" href="<?= base_url() ?>pendaftaran/print/<?= $siswa->Nomor_Pendaftaran ?? '' ?>" class="noPrint">
								Cetak Bukti Pendaftaran
							</a>
						<?php } ?>

					</div>
				</div>
			</form>

		</div>
	</div>
</div>