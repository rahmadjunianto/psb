<h3 >Bukti Pendaftaran</h3>
<br>
Biodata siswa

<table width="100%" border="0">
    <tr>
        <td width="20%">Nomor Pendaftaran</td>
        <td width="5%">:</td>
        <td><?=$siswa->Nomor_Pendaftaran?></td>
    </tr>
    <tr>
        <td width="20%">NIK</td>
        <td width="5%">:</td>
        <td><?=$siswa->NIK?></td>
    </tr>
    <tr>
        <td width="20%">Nama</td>
        <td width="5%">:</td>
        <td><?=$siswa->Nama?></td>
    </tr>
    <tr>
        <td width="20%">Tempat Lahir</td>
        <td width="5%">:</td>
        <td><?=$siswa->Tempat_Lahir?></td>
    </tr>
    <tr>
        <td width="20%">Tanggal Lahir</td>
        <td width="5%">:</td>
        <td><?=$siswa->Tanggal_Lahir?></td>
    </tr>
    <tr>
        <td width="20%">NISN</td>
        <td width="5%">:</td>
        <td><?=$siswa->NISN?></td>
    </tr>
    <tr>
        <td width="20%">Agama</td>
        <td width="5%">:</td>
        <td><?=$siswa->Agama?></td>
    </tr>
    <tr>
        <td width="20%">Jenis Kelamin</td>
        <td width="5%">:</td>
        <td><?=$siswa->Jenis_Kelamin?></td>
    </tr>
    <tr>
        <td width="20%">Sekolah Asal</td>
        <td width="5%">:</td>
        <td><?=$siswa->Sekolah_Asal?></td>
    </tr>
    <tr>
        <td width="20%">Alamat</td>
        <td width="5%">:</td>
        <td><?=$siswa->Alamat?></td>
    </tr>
    <tr>
        <td width="20%">Kecamatan</td>
        <td width="5%">:</td>
        <td><?=$siswa->Kecamatan?></td>
    </tr>
    <tr>
        <td width="20%">Kabupaten / Kota</td>
        <td width="5%">:</td>
        <td><?=$siswa->Kabupaten_Kota?></td>
    </tr>
    <tr>
        <td width="20%">Tahun_Lulus</td>
        <td width="5%">:</td>
        <td><?=$siswa->Tahun_Lulus?></td>
    </tr>
    <tr>
        <td width="20%">Nomor HP</td>
        <td width="5%">:</td>
        <td><?=$siswa->Nomor_Telepon?></td>
    </tr>
</table>
<br>
Biodata Ayah

<table width="100%" border="0">
    <tr>
        <td width="20%">Nama</td>
        <td width="5%">:</td>
        <td><?=$ayah->Nama_Ayah?></td>
    </tr>
    <tr>
        <td width="20%">Tempat Lahir</td>
        <td width="5%">:</td>
        <td><?=$ayah->Tempat_Lahir_Ayah?></td>
    </tr>
    <tr>
        <td width="20%">Tanggal Lahir</td>
        <td width="5%">:</td>
        <td><?=$ayah->Tanggal_Lahir_Ayah?></td>
    </tr>
    <tr>
        <td width="20%">Alamat</td>
        <td width="5%">:</td>
        <td><?=$ayah->Alamat_Ayah?></td>
    </tr>
    <tr>
        <td width="20%">RT</td>
        <td width="5%">:</td>
        <td><?=$ayah->RT?></td>
    </tr>
    <tr>
        <td width="20%">RW</td>
        <td width="5%">:</td>
        <td><?=$ayah->RW?></td>
    </tr>
    <tr>
        <td width="20%">Kecamatan</td>
        <td width="5%">:</td>
        <td><?=$ayah->Kecamatan?></td>
    </tr>
    <tr>
        <td width="20%">Kabupaten / Kota</td>
        <td width="5%">:</td>
        <td><?=$ayah->Kabupaten_Kota?></td>
    </tr>
    <tr>
        <td width="20%">Nomor Telepon</td>
        <td width="5%">:</td>
        <td><?=$ayah->Nomor_Telepon?></td>
    </tr>
</table>
<br>
Biodata Ibu

<table width="100%" border="0">
    <tr>
        <td width="20%">Nama</td>
        <td width="5%">:</td>
        <td><?=$ibu->Nama_Ibu?></td>
    </tr>
    <tr>
        <td width="20%">Tempat Lahir</td>
        <td width="5%">:</td>
        <td><?=$ibu->Tempat_Lahir_Ibu?></td>
    </tr>
    <tr>
        <td width="20%">Tanggal Lahir</td>
        <td width="5%">:</td>
        <td><?=$ibu->Tanggal_Lahir_Ibu?></td>
    </tr>
    <tr>
        <td width="20%">Alamat</td>
        <td width="5%">:</td>
        <td><?=$ibu->Alamat?></td>
    </tr>
    <tr>
        <td width="20%">RT</td>
        <td width="5%">:</td>
        <td><?=$ibu->RT?></td>
    </tr>
    <tr>
        <td width="20%">RW</td>
        <td width="5%">:</td>
        <td><?=$ibu->RW?></td>
    </tr>
    <tr>
        <td width="20%">Kecamatan</td>
        <td width="5%">:</td>
        <td><?=$ibu->Kecamatan?></td>
    </tr>
    <tr>
        <td width="20%">Kabupaten / Kota</td>
        <td width="5%">:</td>
        <td><?=$ibu->Kabupaten_Kota?></td>
    </tr>
    <tr>
        <td width="20%">Nomor Telepon</td>
        <td width="5%">:</td>
        <td><?=$ibu->Nomor_Telepon?></td>
    </tr>
</table>
<script>
    window.print()
</script>