<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pendaftaran_model extends CI_Model
{

  public function insert($data)
  {
    $this->db->insert('tb_pendaftaran', $data);
  }
  public function get_all()
  {
    $query = $this->db->get('tb_pendaftaran');
    return $query->result_array();
  }
  function get_no_pendaftaran()
  {
    $year = date('Y');
    $cd = $this->db->query("SELECT MAX(RIGHT(Nomor_Pendaftaran,5)) AS Nomor_Pendaftaran FROM tb_pendaftaran where substr(Nomor_Pendaftaran,2,4)=$year");
    $kd = "";
    if ($cd->num_rows() > 0) {
      foreach ($cd->result() as $k) {
        $NoUrut = (int) substr($k->Nomor_Pendaftaran, 1, 7);
        $tmp = (int) $NoUrut+ 1;
        $kd = sprintf("%04s", $tmp);
      }
    } else {
      $kd = "0001";
    }
    return "P" .$year. $kd;
  }
}
