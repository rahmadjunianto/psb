/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100418 (10.4.18-MariaDB)
 Source Host           : localhost:3306
 Source Schema         : tes

 Target Server Type    : MySQL
 Target Server Version : 100418 (10.4.18-MariaDB)
 File Encoding         : 65001

 Date: 13/08/2023 16:59:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_admin
-- ----------------------------
DROP TABLE IF EXISTS `tb_admin`;
CREATE TABLE `tb_admin`  (
  `Id_Admin` int NOT NULL AUTO_INCREMENT,
  `Nama_Admin` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`Id_Admin`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 123456798 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_admin
-- ----------------------------
INSERT INTO `tb_admin` VALUES (123456789, 'Admin SMP Unggulan Al-Falah', 'Desi Fitriani', 'A20230000');
INSERT INTO `tb_admin` VALUES (123456797, '085853335400', 'madjun', '1');

-- ----------------------------
-- Table structure for tb_orang_tua_ayah
-- ----------------------------
DROP TABLE IF EXISTS `tb_orang_tua_ayah`;
CREATE TABLE `tb_orang_tua_ayah`  (
  `Nama_Ayah` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Tempat_Lahir_Ayah` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Tanggal_Lahir_Ayah` date NOT NULL,
  `Alamat_Ayah` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `RT` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `RW` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Kecamatan` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Kabupaten_Kota` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Nomor_Telepon` int NOT NULL,
  `Nomor_Pendaftaran` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  INDEX `Nomor_Pendaftaran`(`Nomor_Pendaftaran` ASC) USING BTREE,
  CONSTRAINT `tb_orang_tua_ayah_ibfk_1` FOREIGN KEY (`Nomor_Pendaftaran`) REFERENCES `tb_pendaftaran` (`Nomor_Pendaftaran`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_orang_tua_ayah
-- ----------------------------
INSERT INTO `tb_orang_tua_ayah` VALUES ('a', 'a', '2023-08-12', 'Perum Griya Intan Permai Blok IB 12 RT 04 RW 03', 'a', 'a', 'a', 'Kota Kediriqq', 2147483647, 'P20230001');

-- ----------------------------
-- Table structure for tb_orang_tua_ibu
-- ----------------------------
DROP TABLE IF EXISTS `tb_orang_tua_ibu`;
CREATE TABLE `tb_orang_tua_ibu`  (
  `Nama_Ibu` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Tempat_Lahir_Ibu` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Tanggal_Lahir_Ibu` date NOT NULL,
  `Alamat` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `RT` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `RW` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Kecamatan` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Kabupaten_Kota` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Nomor_Telepon` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Nomor_Pendaftaran` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  INDEX `Nomor_Pendaftaran`(`Nomor_Pendaftaran` ASC) USING BTREE,
  CONSTRAINT `tb_orang_tua_ibu_ibfk_1` FOREIGN KEY (`Nomor_Pendaftaran`) REFERENCES `tb_pendaftaran` (`Nomor_Pendaftaran`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_orang_tua_ibu
-- ----------------------------
INSERT INTO `tb_orang_tua_ibu` VALUES ('i', 'i', '2023-08-12', 'Perum Griya Intan Permai Blok IB 12 RT 04 RW 03', 'i', 'i', 'i', 'Kota Kediriaa', '085853335400', 'P20230001');

-- ----------------------------
-- Table structure for tb_pendaftaran
-- ----------------------------
DROP TABLE IF EXISTS `tb_pendaftaran`;
CREATE TABLE `tb_pendaftaran`  (
  `Nomor_Pendaftaran` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Nama` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `NIK` int NOT NULL,
  `Tempat_Lahir` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Tanggal_Lahir` date NOT NULL,
  `NISN` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Agama` enum('Islam','Kristen','Hindu','Budha','Konghuchu') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Sekolah_Asal` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Jenis_Kelamin` enum('Laki-Laki','Perempuan') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Alamat` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Kecamatan` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Kabupaten_Kota` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Tahun_Lulus` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Id_Admin` int NOT NULL AUTO_INCREMENT,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Nomor_Pendaftaran`) USING BTREE,
  INDEX `Id_Admin`(`Id_Admin` ASC) USING BTREE,
  CONSTRAINT `tb_pendaftaran_ibfk_1` FOREIGN KEY (`Id_Admin`) REFERENCES `tb_admin` (`Id_Admin`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 123456798 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_pendaftaran
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
